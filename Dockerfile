FROM golang:1.20 AS builder
WORKDIR /usr/src/app
ENV GOPROXY=https://goproxy.io,direct
COPY . .
RUN go mod tidy -x
RUN CGO_ENABLED=1 go build -ldflags="-s -w" -o main .

FROM ubuntu:latest AS runner
COPY --from=builder /usr/src/app/main .
EXPOSE 50051
CMD ["./main"]
