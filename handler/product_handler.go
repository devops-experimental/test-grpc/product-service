package handler

import (
	"context"
	"github.com/micro/micro/v3/service/logger"
	pb "gitlab.com/devops-experimental/test-grpc/proto"
)

type ProductHandler struct{}

func (s *ProductHandler) GetProductById(ctx context.Context, request *pb.GetProductByIdRequest, product *pb.Product) error {
	logger.Info("Received: %v", request.GetProductId())
	product.Id = request.ProductId
	product.Name = "Dummy Product"
	return nil
}
