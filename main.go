package main

import (
	"fmt"
	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
	pb "gitlab.com/devops-experimental/test-grpc/proto"
	"product_service/handler"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("product-service"),
	)

	// Register Handler
	pb.RegisterProductServiceHandler(srv.Server(), &handler.ProductHandler{})

	fmt.Println("Product service is running...")

	// Run the service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
